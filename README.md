# L6_Git quizz Assignment

 The PiHAT chosen for our project is an uninterupted power supply, that contains status LEDS for the battery level indicator.
 
 Bill of Materials:
 - 2x 18k resitors
 - 2x 82K resitors
 - 1 x 68k, 27k, 15k, 39k, 12k, 27k, 22k and 47k resitors
 - 6 x opamps 
 - 6 x LEDS
 - 4 x 0.14k resitors
 - 1 x 0.07k resitors
 - 1x 0.155k resitors

 Description of status LEDS:
This circuit is going to be integrated with the battery sensing(amplifier) circuit to accurately represent and indicate the
battery levels of the lithium ion battery used to run the UPS. For this section, although the battery will, over time decrease
in voltage outputs, this LED circuit is assuming an input of voltage of either 5V or 0V, or “high” and “low”. This will be
achieved via the amplifier in the previous section. 6 standard 3mm LEDs, of 4 different colours were chosen as these had
constant forward voltage and current ratings. They were chosen as they are smaller when compared to the 5mm LEDs suggested
previously and takes up less space. Each LED has a forward current of 20mA and has different forward voltages depending on the
color of the LED.
